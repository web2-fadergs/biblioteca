<!DOCTYPE html>

<!-- this is header -->
<nav class="navbar navbar-default navbar-static-top navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/biblioteca">Projeto Biblioteca</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            
            	<li class="dropdown">
					<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true">
						Cadastros
						<span class="caret" ></span>
					</a>					
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="bibliotecas">Bibliotecas</a></li>
						<li><a class="dropdown-item" href="funcionarios">Funcionários</a></li>
                		<li><a class="dropdown-item" href="alunos">Alunos</a></li>
					</ul>
				</li>
								
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true">
						Biblioteca
						<span class="caret" ></span>
					</a>					
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="livros">Livros</a></li>
						<li><a class="dropdown-item" href="categorias">Categorias</a></li>
						<li><a class="dropdown-item" href="emprestimos">Empréstimo</a></li>
					</ul>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true">
						Relatórios
						<span class="caret" ></span>
					</a>					
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="relatorios">Movimentações de Livros</a></li>
						<li><a class="dropdown-item" href="relatorios?action=aluno">Histórico por Aluno</a></li>
						<li><a class="dropdown-item" href="relatorios?action=contador">Quantidade de Livros com Alunos</a></li>
					</ul>
				</li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>