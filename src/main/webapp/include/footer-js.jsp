<script type="text/javascript" src="/biblioteca/webjars/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="/biblioteca/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/biblioteca/js/jquery.mask.min.js"></script>

<script type="text/javascript">
    //<![CDATA[

    $(".currency").mask('000.000.000.000.000,00', {reverse: true});
    $(".integer").mask('000.000.000.000.000', {reverse: true});
    $(".phone").mask("(99) 99999-9999");

    window.setTimeout(function() {
        $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);

    //]]>
</script>