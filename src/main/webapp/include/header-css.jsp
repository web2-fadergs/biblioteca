<title>WEB2 :: Biblioteca</title>
<!-- this is header-css -->
<link rel="stylesheet" type="text/css" href="/biblioteca/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/biblioteca/webjars/font-awesome/4.7.0/css/font-awesome.css" />

<style>
	
	.navbar {
		margin-bottom : 0px;
	}

	.cover{ 
		background: url(img/background.jpg) no-repeat center center fixed; 
       	background-position: center;
       	background-repeat: no-repeat;
       	background-size: cover;
       	-webkit-background-size: cover;
       	-moz-background-size: cover;
       	-o-background-size: cover;
       	min-height: 679px;
       	height: 91%;
	}


</style>
