<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />
	  	
<div class="container">
    <div class="col-lg-12">
        <h1>Empr�stimos Ativos</h1>
	</div>
	
	<div class="col-lg-12">
        <div class="button-group">
            <a href="/biblioteca/emprestimos?action=form" class="btn btn-primary">
            	<i class="fa fa-plus" aria-hidden="true"></i> Incluir
            </a>
        </div>
    </div>
    
	<div class=" col-lg-12">
		<div class="table-responsive">
			<table class="table table-striped">
			    <thead>
				    <tr>
				        <th>Livro</th>
				        <th>Aluno</th>
				        <th>Valor</th>
				        <th>Data Retirada</th>
				        <th>Previs�o Entrega</th>
				        <th></th>
				    </tr>
			    </thead>
			    <tbody>
			    	<c:forEach var="emprestimo" items="${emprestimos}">
					    <tr>
					        <td>${emprestimo.livro.titulo}</td>
					        <td>${emprestimo.aluno.nome}</td>
					        <td class="currency">${String.format( "%.2f", emprestimo.livro.valor )}</td>
					        <td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${emprestimo.retirada}" /></td>
					        <td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${emprestimo.previsao}" /></td>
					        
					        <td class="col-sm-1">
								<form id="form-empresta" method="POST" action="/biblioteca/emprestimos" style="margin: 0px;">
									<input type="hidden" name="action" id="form-action" value="devolver" />
									<input type="hidden" name="id_empresta" value="${emprestimo.idEmpresta}" /> 
									<div class="input-group">
								        <div class="input-group-btn">
								            <button type="submit" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i></button>
								            <button type="button" class="btn btn-success" onclick="renovar(${emprestimo.idEmpresta});"><i class="fa fa-refresh" aria-hidden="true"></i></button>
								        </div>
							    	</div>
						    	</form>
							</td>
					    </tr>
			    	</c:forEach>
			    </tbody>
			</table>
		</div>
	</div>
</div>

<form id="form-renovar" method="POST" action="/biblioteca/emprestimos" style="margin: 0px;">
	<input type="hidden" name="action" value="renovar" />
	<input type="hidden" name="id_empresta" id="renovar-id" value="" />
</form>

<jsp:include page="../include/footer-js.jsp" />

<script>

	function renovar(id) {
		$("#renovar-id").val(id);
		$("#form-renovar").submit();
	}


    $('#deleteBiblioteca').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var idcategoria = button.data('idcategoria');
        var nome = button.data('nome');

        var modal = $(this)
        modal.find('#delete-id').val(idcategoria);
        modal.find('#categoria-nome').text(nome);
    });

</script>

</body>
</html>

