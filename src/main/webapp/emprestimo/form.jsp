<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />

<div class="container">

	<div class="col-lg-12">
		<h1>Empréstimo de Livros</h1>
    </div>
    
    <div class="col-lg-12">
        <form id="form-emprestimo" method="POST" action="/biblioteca/emprestimos">
        	<input type="hidden" name="action" value="salvar" />

            <div class="col-md-6">
                <div class="form-group">
                    <h6>Livro</h6>
                    <select class="form-control" name="id_livro">
                    	<c:forEach var="livro" items="${livros}">
                    		<option value="${livro.idLivro}">${livro.titulo}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <h6>Aluno</h6>
                    <select class="form-control" name="id_aluno">
                    	<c:forEach var="aluno" items="${alunos}">
                    		<option value="${aluno.idAluno}">${aluno.nome}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <h6>&nbsp;</h6>
                <div class="pull-right">
                	<a href="/biblioteca/emprestimos" class="btn btn-link">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>

</div>

<jsp:include page="../include/footer-js.jsp" />
</body>
</html>