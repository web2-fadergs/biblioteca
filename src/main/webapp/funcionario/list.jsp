<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />
	  	
<div class="container">
    <div class="col-lg-12">
        <h1>Funcionarios</h1>
	</div>
	
	<div class=" col-md-12">
        <form id="form-pesquisa" method="POST" action="/biblioteca/funcionarios">
			<input type="hidden" name="action" value="listar">
	        <div class="input-group">
		        <input autofocus="true" type="text" class="form-control" placeholder="Pesquisar..." name="pesquisa" id="pesquisa"/>
		        <div class="input-group-btn">
		            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Pesquisar</button>
		        </div>
	    	</div>
        </form>
        <br /><br /><br />
    </div>
	
	<div class="col-lg-12">
        <div class="button-group">
            <a href="/biblioteca/funcionarios?action=form" class="btn btn-primary">
            	<i class="fa fa-plus" aria-hidden="true"></i> Incluir
            </a>
        </div>
    </div>
    
	<div class=" col-lg-12">
		<div class="table-responsive">
			<table class="table table-striped">
			    <thead>
				    <tr>
				        <th class="col-sm-1">Matricula</th>
				        <th>Nome</th>
				        <th>Endere�o</th>
				        <th>Telefone</th>
				        <th>Sal�rio</th>
				        <th>Biblioteca</th>
				        <th></th>
				    </tr>
			    </thead>
			    <tbody>
			    	<c:forEach var="funcionario" items="${funcionarios}">
					    <tr>
					        <td class="col-sm-1">${funcionario.idFuncionario}</td>
					        <td>${funcionario.nome}</td>
					        <td>${funcionario.endereco}</td>
					        <td class="phone">${funcionario.telefone}</td>
					        <td class="currency">${String.format( "%.2f", funcionario.salario )}</td>
					        <td>${funcionario.biblioteca.nome}</td>
					        <td class="col-sm-1">
								<form id="form-pesquisa" method="POST" action="/biblioteca/funcionarios" style="margin: 0px;">
									<input type="hidden" name="action" value="editar" />
									<input type="hidden" name="id_funcionario" value="${funcionario.idFuncionario}" /> 
									<div class="input-group">
								        <div class="input-group-btn">
								            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
								            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteFuncionario"
								            	data-idfuncionario="${funcionario.idFuncionario}" data-nome="${funcionario.nome}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i></button>
								        </div>
							    	</div>
						    	</form>
							</td>
					    </tr>
			    	</c:forEach>
			    </tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="deleteFuncionario" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title popup-title" id="deleteModalLabel">Deseja Excluir este Funcion�rio?</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3><span id="funcionario-nome"></span></h3>
            </div>
            <div class="modal-footer">

                <form id="form-delete" action="/biblioteca/funcionarios" method="post">
                	<input type="hidden" name="action" value="deletar" />
					<input type="hidden" name="id_funcionario" id="delete-id" /> 
					
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </form>

            </div>
        </div>
    </div>
</div>

<jsp:include page="../include/footer-js.jsp" />

<script>

    $('#deleteFuncionario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var idfuncionario = button.data('idfuncionario');
        var nome = button.data('nome');

        var modal = $(this)
        modal.find('#delete-id').val(idfuncionario);
        modal.find('#funcionario-nome').text(nome);
    });

</script>

</body>
</html>

