<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />

<div class="container">

	<div class="col-lg-12">
		<c:choose>
		   <c:when test="${funcionario.idFuncionario == null}"><h1>Cadastrar Funcionario</h1></c:when>
		   <c:otherwise><h1>Editar Funcionario</h1></c:otherwise>
		</c:choose>
    </div>
    
    <div class="col-lg-12">
    	<c:if test="${hasFieldErrors}">
         <div class="alert alert-danger">
             Dados Inv�lidos!
             <ul>
                 <li>Erro!</li>
             </ul>
         </div>
        </c:if>

        <form id="form-funcionario" method="POST" action="/biblioteca/funcionarios">
        	<input type="hidden" name="action" value="salvar" />
            <input type="hidden" name="id_funcionario" value="${funcionario.idFuncionario}"/>

            <div class="col-md-6">
                <h6>Nome</h6>
                <input autofocus="true" type="text" class="form-control" name="nome" id="nome" value="${funcionario.nome}" required="true" />
            </div>
            <div class="col-md-6">
                <h6>Endere�o</h6>
                <input type="text" class="form-control" name="endereco" id="endereco" value="${funcionario.endereco}" required="true" />
            </div>
            <div class="col-md-4">
                <h6>Telefone</h6>
                <input type="text" class="form-control phone" name="telefone" id="telefone" value="${funcionario.telefone}" required="true" />
            </div>
            <div class="col-md-4">
                <h6>Sal�rio</h6>
                <input type="text" class="form-control currency" name="salario" id="salario" value="${String.format( '%.2f', funcionario.salario )}" required="true" />
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <h6>Biblioteca</h6>
                    <select class="form-control" name="id_biblioteca">
                    	<c:forEach var="biblioteca" items="${bibliotecas}">
                    		<c:choose>
							   	<c:when test="${biblioteca.idBiblioteca == funcionario.biblioteca.idBiblioteca}">
							   		<option value="${biblioteca.idBiblioteca}" selected>${biblioteca.nome}</option>
							   	</c:when>
							   	<c:otherwise>
							   		<option value="${biblioteca.idBiblioteca}">${biblioteca.nome}</option>
							   	</c:otherwise>
							</c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            
            <div class="col-md-12">
                <h6>&nbsp;</h6>
                <div class="pull-right">
                    <a href="/biblioteca/funcionarios" class="btn btn-link">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>

</div>

<jsp:include page="../include/footer-js.jsp" />
</body>
</html>