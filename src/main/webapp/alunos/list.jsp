<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />
	  	
<div class="container">
    <div class="col-lg-12">
        <h1>Alunos</h1>
	</div>
	
	<div class=" col-md-12">
        <form id="form-pesquisa" method="POST" action="/biblioteca/alunos">
			<input type="hidden" name="action" value="listar">
	        <div class="input-group">
		        <input autofocus="true" type="text" class="form-control" placeholder="Pesquisar..." name="pesquisa" id="pesquisa"/>
		        <div class="input-group-btn">
		            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Pesquisar</button>
		        </div>
	    	</div>
        </form>
        <br /><br /><br />
    </div>
	
	<div class="col-lg-12">
        <div class="button-group">
            <a href="/biblioteca/alunos?action=form" class="btn btn-primary">
            	<i class="fa fa-plus" aria-hidden="true"></i> Incluir
            </a>
        </div>
    </div>
    
	<div class=" col-lg-12">
		<div class="table-responsive">
			<table class="table table-striped">
			    <thead>
				    <tr>
				        <th class="col-sm-1">Matricula</th>
				        <th>Nome</th>
				        <th>Endere�o</th>
				        <th>Situa��o</th>
				        <th></th>
				    </tr>
			    </thead>
			    <tbody>
			    	<c:forEach var="aluno" items="${alunos}">
					    <tr>
					        <td class="col-sm-1">${aluno.idAluno}</td>
					        <td>${aluno.nome}</td>
					        <td>${aluno.endereco}</td>
					        <td>
					        	<c:choose>
								   <c:when test="${aluno.situacao}">
								   		<a href="emprestimos">Ativo</a>
								   </c:when>
								   <c:otherwise>Inativo</c:otherwise>
								</c:choose>
							</td>
							<td class="col-sm-1">
								<form id="form-pesquisa" method="POST" action="/biblioteca/alunos" style="margin: 0px;">
									<input type="hidden" name="action" value="editar" />
									<input type="hidden" name="id_aluno" value="${aluno.idAluno}" /> 
									<div class="input-group">
								        <div class="input-group-btn">
								            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
								        </div>
							    	</div>
						    	</form>
							</td>
					    </tr>
			    	</c:forEach>
			    </tbody>
			</table>
		</div>
	</div>
</div>

<jsp:include page="../include/footer-js.jsp" />

</body>
</html>

