<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />

<div class="container">

	<div class="col-lg-12">
		<c:choose>
		   <c:when test="${biblioteca.idBiblioteca == null}"><h1>Cadastrar Biblioteca</h1></c:when>
		   <c:otherwise><h1>Editar Biblioteca</h1></c:otherwise>
		</c:choose>
    </div>
    
    <div class="col-lg-12">
    	<c:if test="${hasFieldErrors}">
         <div class="alert alert-danger">
             Dados Inv�lidos!
             <ul>
                 <li>Erro!</li>
             </ul>
         </div>
        </c:if>

        <form id="form-biblioteca" method="POST" action="/biblioteca/bibliotecas">
        	<input type="hidden" name="action" value="salvar" />
            <input type="hidden" name="id_biblioteca" value="${biblioteca.idBiblioteca}"/>

            <div class="col-md-6">
                <h6>Nome</h6>
                <input autofocus="true" type="text" class="form-control" name="nome" id="nome" value="${biblioteca.nome}" required="true" />
            </div>
            <div class="col-md-6">
                <h6>Endere�o</h6>
                <input type="text" class="form-control" name="endereco" id="endereco" value="${biblioteca.endereco}" required="true" />
            </div>
            <div class="col-md-12">
                <h6>&nbsp;</h6>
                <div class="pull-right">
                    <a href="/biblioteca/bibliotecas" class="btn btn-link">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>

</div>

<jsp:include page="../include/footer-js.jsp" />
</body>
</html>