<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />

<div class="container">

	<div class="col-lg-12">
		<c:choose>
		   <c:when test="${livro.idLivro == null}"><h1>Cadastrar Livro</h1></c:when>
		   <c:otherwise><h1>Editar Livro</h1></c:otherwise>
		</c:choose>
    </div>
    
    <div class="col-lg-12">
    	<c:if test="${hasFieldErrors}">
         <div class="alert alert-danger">
             Dados Inválidos!
             <ul>
                 <li>Erro!</li>
             </ul>
         </div>
        </c:if>

        <form id="form-livro" method="POST" action="/biblioteca/livros">
        	<input type="hidden" name="action" value="salvar" />
            <input type="hidden" name="id_livro" value="${livro.idLivro}"/>
            <input type="hidden" name="situacao" value="${livro.situacao}"/>

            <div class="col-md-6">
                <h6>Titulo</h6>
                <input autofocus="true" type="text" class="form-control" name="titulo" id="titulo" value="${livro.titulo}" required="true" />
            </div>
            <div class="col-md-6">
                <h6>Editora</h6>
                <input type="text" class="form-control" name="editora" id="editora" value="${livro.editora}" required="true" />
            </div>
            <div class="col-md-4">
                <h6>Valor</h6>
                <input type="text" class="form-control currency" name="valor" id="valor" value="${String.format( '%.2f', livro.valor )}" required="true" />
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <h6>Categoria</h6>
                    <select class="form-control" name="id_categoria">
                    	<c:forEach var="categoria" items="${categorias}">
                    		<c:choose>
							   	<c:when test="${categoria.idCategoria == livro.categoria.idCategoria}">
							   		<option value="${categoria.idCategoria}" selected>${categoria.descricao}</option>
							   	</c:when>
							   	<c:otherwise>
							   		<option value="${categoria.idCategoria}">${categoria.descricao}</option>
							   	</c:otherwise>
							</c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <h6>Biblioteca</h6>
                    <select class="form-control" name="id_biblioteca">
                    	<c:forEach var="biblioteca" items="${bibliotecas}">
                    		<c:choose>
							   	<c:when test="${biblioteca.idBiblioteca == livro.biblioteca.idBiblioteca}">
							   		<option value="${biblioteca.idBiblioteca}" selected>${biblioteca.nome}</option>
							   	</c:when>
							   	<c:otherwise>
							   		<option value="${biblioteca.idBiblioteca}">${biblioteca.nome}</option>
							   	</c:otherwise>
							</c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            
            <div class="col-md-12">
                <h6>&nbsp;</h6>
                <div class="pull-right">
                    <a href="/biblioteca/livros" class="btn btn-link">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </form>
    </div>

</div>

<jsp:include page="../include/footer-js.jsp" />
</body>
</html>