<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />
	  	
<div class="container">
    <div class="col-lg-12">
        <h1>Movimenta��es de Livros</h1>
	</div>
	
	<div class=" col-md-12">
        <form id="form-pesquisa" method="POST" action="/biblioteca/relatorios">
	        <div class="input-group">
		        <select class="form-control" name="id_livro">
		        	<option selected value="">Filtrar Livro...</option>
					<c:forEach var="filtro" items="${todosLivros}">
						<option value="${filtro.idLivro}">${filtro.titulo}</option>
					</c:forEach>
				</select>		        
		        <div class="input-group-btn">
		            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Pesquisar</button>
		        </div>
	    	</div>
        </form>
        <br /><br /><br />
    </div>
	
	<div class=" col-lg-12">
	
		<c:forEach var="livro" items="${movimentacaoLivros}">
			<div class="panel panel-default">
				<div class="panel-body">
				
					<table class="table table-striped">
					    <thead>
						    <tr>
						        <th class="col-sm-1">C�digo</th>
						        <th>T�tulo</th>
						        <th>Editora</th>
						        <th>Categoria</th>
						        <th>Biblioteca</th>
						        <th>Situa��o</th>
						    </tr>
					    </thead>
					    <tbody>
				    	    <tr>
						        <td class="col-sm-1">${livro.idLivro}</td>
						        <td>${livro.titulo}</td>
						        <td>${livro.editora}</td>
						        <td>${livro.categoria.descricao}</td>
						        <td>${livro.biblioteca.nome}</td>
						        <td>
						        	<c:choose>
									   <c:when test="${livro.situacao}">
									   		Dispon�vel
									   </c:when>
									   <c:otherwise>Emprestado</c:otherwise>
									</c:choose>
								</td>
						    </tr>				    	
					    </tbody>
					</table>
				
					<div class="panel panel-default">
						<div class="panel-body">
						
							<table class="table table-striped">
							    <thead>
								    <tr>
								        <th>Aluno</th>
										<th>Situa��o Aluno</th>
								        <th>Data Retirada</th>
								        <th>Previs�o Entrega</th>
								        <th>Entrega</th>
								    </tr>
							    </thead>
							    <tbody>
							    	<c:forEach var="emprestimo" items="${livro.emprestimos}">
									    <tr>
									        <td>${emprestimo.aluno.nome}</td>
									        <td>
									        	<c:choose>
												   <c:when test="${emprestimo.aluno.situacao}">
												   		Ativo												   	
												   </c:when>
												   <c:otherwise>Inativo</c:otherwise>
												</c:choose>
											</td>
									        <td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${emprestimo.retirada}" /></td>
									        <td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${emprestimo.previsao}" /></td>
									        <td><fmt:formatDate pattern = "dd/MM/yyyy" value = "${emprestimo.entrega}" /></td>
									    </tr>
							    	</c:forEach>
							    </tbody>
							</table>
						
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
		
	</div>
</div>

<jsp:include page="../include/footer-js.jsp" />

</body>
</html>

