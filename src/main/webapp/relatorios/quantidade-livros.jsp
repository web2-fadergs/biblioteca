<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<jsp:include page="../include/header-css.jsp" />
</head>
<body>
<jsp:include page="../include/navbar.jsp" />
	  	
<div class="container">
    <div class="col-lg-12">
        <h1>Quantidade de Livros com Alunos</h1>
	</div>
	
	<div class=" col-lg-12">
		<h3>Alunos Ativos</h3>
		<div class="table-responsive">
			<table class="table table-striped">
			    <thead>
				    <tr>
				        <th class="col-sm-1">Matricula</th>
				        <th class="col-md-4">Nome</th>
				        <th class="col-sm-6">Endere�o</th>
				        <th class="col-sm-2">Livros</th>
				    </tr>
			    </thead>
			    <tbody>
			    	<c:forEach var="ativo" items="${alunosAtivos}">
					    <tr>
					        <td class="col-sm-1">${ativo.idAluno}</td>
					        <td>${ativo.nome}</td>
					        <td>${ativo.endereco}</td>
					        <td>${ativo.quantidadeLivros}</td>
					    </tr>
			    	</c:forEach>
			    </tbody>
			</table>
		</div>
	</div>
	
	<div class=" col-lg-12">
		<h3>Alunos Inativos</h3>
		<div class="table-responsive">
			<table class="table table-striped">
			    <thead>
				    <tr>
				        <th class="col-sm-1">Matricula</th>
				        <th class="col-md-4">Nome</th>
				        <th class="col-sm-6">Endere�o</th>
				        <th class="col-sm-2">Livros</th>
				    </tr>
			    </thead>
			    <tbody>
			    	<c:forEach var="inativo" items="${alunosInativos}">
					    <tr>
					        <td class="col-sm-1">${inativo.idAluno}</td>
					        <td>${inativo.nome}</td>
					        <td>${inativo.endereco}</td>
					        <td>${inativo.quantidadeLivros}</td>
					    </tr>
			    	</c:forEach>
			    </tbody>
			</table>
		</div>
	</div>
</div>

<jsp:include page="../include/footer-js.jsp" />

</body>
</html>

