package com.fadergs.web2.biblioteca.util.log;

public enum LogLevel {
	INFO, DEBUG, TRACE, ERROR;
}
