package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.BibliotecaDAO;
import com.fadergs.web2.biblioteca.entity.Biblioteca;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorBiblioteca
 */
@WebServlet("/bibliotecas")
public class BibliotecaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(BibliotecaServlet.class);

	private static final String LIST_PAGE = "/biblioteca/list.jsp";
	private static final String FORM_PAGE = "/biblioteca/form.jsp";

	public BibliotecaServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Biblioteca biblioteca = new Biblioteca();

		if ("salvar".equals(action)) {
			if (request.getParameter("id_biblioteca") != null && !"".equals(request.getParameter("id_biblioteca"))) {
				biblioteca.setIdBiblioteca(Long.valueOf(request.getParameter("id_biblioteca")));
			}
			biblioteca.setNome(request.getParameter("nome"));
			biblioteca.setEndereco(request.getParameter("endereco"));
			BibliotecaDAO.salvar(biblioteca);
		}
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		List<Biblioteca> bibliotecas = null;
		
		RequestDispatcher rd;
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);			
		} else if ("editar".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
			
			Long idBiblioteca = Long.valueOf(request.getParameter("id_biblioteca"));			
			Biblioteca biblioteca = BibliotecaDAO.buscarId(idBiblioteca);
			request.setAttribute("biblioteca", biblioteca);
		} else if ("deletar".equals(action)) {
			rd = request.getRequestDispatcher(LIST_PAGE);
			
			Long idBiblioteca = Long.valueOf(request.getParameter("id_biblioteca"));
			BibliotecaDAO.deletar(idBiblioteca);
			
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);
		}

		if ("listar".equals(action)) {
			String pesquisa = request.getParameter("pesquisa");
			bibliotecas = BibliotecaDAO.buscarNome(pesquisa);
		} else {
			bibliotecas = BibliotecaDAO.buscarTodos();
		}
		
		request.setAttribute("bibliotecas", bibliotecas);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}
}