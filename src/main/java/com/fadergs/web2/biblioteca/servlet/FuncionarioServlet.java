package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.BibliotecaDAO;
import com.fadergs.web2.biblioteca.dao.FuncionarioDAO;
import com.fadergs.web2.biblioteca.entity.Funcionario;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorFuncionario
 */
@WebServlet("/funcionarios")
public class FuncionarioServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(FuncionarioServlet.class);

	private static final String LIST_PAGE = "/funcionario/list.jsp";
	private static final String FORM_PAGE = "/funcionario/form.jsp";

	public FuncionarioServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Funcionario funcionario = new Funcionario();

		if ("salvar".equals(action)) {
			if (request.getParameter("id_funcionario") != null && !"".equals(request.getParameter("id_funcionario"))) {
				funcionario.setIdFuncionario(Long.valueOf(request.getParameter("id_funcionario")));
			}
			
			Double salario = null;
			if (request.getParameter("salario") != null) {
				String salariostr = request.getParameter("salario").replaceAll("[.]", "").replaceAll(",", ".");
				salario = ("".equals(salariostr)) ? 0d : Double.valueOf(salariostr);
			}			
			funcionario.setNome(request.getParameter("nome"));
			funcionario.setEndereco(request.getParameter("endereco"));
			funcionario.setTelefone(request.getParameter("telefone"));
			funcionario.setSalario(salario);
			funcionario.getBiblioteca().setIdBiblioteca(Long.valueOf(request.getParameter("id_biblioteca")));
			
			FuncionarioDAO.salvar(funcionario);
		}
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		List<Funcionario> funcionarios = null;
		
		RequestDispatcher rd;
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);			
		} else if ("editar".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
			
			Long idFuncionario = Long.valueOf(request.getParameter("id_funcionario"));			
			Funcionario funcionario = FuncionarioDAO.buscarId(idFuncionario);
			request.setAttribute("funcionario", funcionario);
		} else if ("deletar".equals(action)) {
			rd = request.getRequestDispatcher(LIST_PAGE);
			
			Long idFuncionario = Long.valueOf(request.getParameter("id_funcionario"));
			FuncionarioDAO.deletar(idFuncionario);
			
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);
		}

		if ("listar".equals(action)) {
			String pesquisa = request.getParameter("pesquisa");
			funcionarios = FuncionarioDAO.buscarNome(pesquisa);
		} else {
			funcionarios = FuncionarioDAO.buscarTodos();
		}
		
		request.setAttribute("bibliotecas", BibliotecaDAO.buscarTodos());
		request.setAttribute("funcionarios", funcionarios);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}
}