package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.BibliotecaDAO;
import com.fadergs.web2.biblioteca.dao.CategoriaDAO;
import com.fadergs.web2.biblioteca.dao.LivroDAO;
import com.fadergs.web2.biblioteca.entity.Livro;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorLivro
 */
@WebServlet("/livros")
public class LivroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(LivroServlet.class);

	private static final String LIST_PAGE = "/livro/list.jsp";
	private static final String FORM_PAGE = "/livro/form.jsp";

	public LivroServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Livro livro = new Livro();

		if ("salvar".equals(action)) {
			Boolean situacao = true;
			if (request.getParameter("id_livro") != null && !"".equals(request.getParameter("id_livro"))) {
				livro.setIdLivro(Long.valueOf(request.getParameter("id_livro")));
			}
			Double valor = null;
			if (request.getParameter("valor") != null) {
				String valorstr = request.getParameter("valor").replaceAll("[.]", "").replaceAll(",", ".");
				valor = ("".equals(valorstr)) ? 0d : Double.valueOf(valorstr);
			}	
			livro.setTitulo(request.getParameter("titulo"));
			livro.setEditora(request.getParameter("editora"));
			livro.setSituacao(situacao);
			livro.setValor(valor);
			livro.getBiblioteca().setIdBiblioteca(Long.valueOf(request.getParameter("id_biblioteca")));
			livro.getCategoria().setIdCategoria(Long.valueOf(request.getParameter("id_categoria")));
			
			LivroDAO.salvar(livro);
		}
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		List<Livro> livros = null;
		
		RequestDispatcher rd;
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);			
		} else if ("editar".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
			
			Long idLivro = Long.valueOf(request.getParameter("id_livro"));			
			Livro livro = LivroDAO.buscarId(idLivro);
			request.setAttribute("livro", livro);
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);
		}

		if ("listar".equals(action)) {
			String pesquisa = request.getParameter("pesquisa");
			livros = LivroDAO.buscarTitulo(pesquisa);
		} else {
			livros = LivroDAO.buscarTodos();
		}
		
		request.setAttribute("bibliotecas", BibliotecaDAO.buscarTodos());
		request.setAttribute("categorias", CategoriaDAO.buscarTodos());
		request.setAttribute("livros", livros);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}

}