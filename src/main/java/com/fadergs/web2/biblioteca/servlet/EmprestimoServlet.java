package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.fadergs.web2.biblioteca.dao.AlunoDAO;
import com.fadergs.web2.biblioteca.dao.EmprestaDAO;
import com.fadergs.web2.biblioteca.dao.LivroDAO;
import com.fadergs.web2.biblioteca.entity.Empresta;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorLivro
 */
@WebServlet("/emprestimos")
public class EmprestimoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(EmprestimoServlet.class);

	private static final String LIST_PAGE = "/emprestimo/list.jsp";
	private static final String FORM_PAGE = "/emprestimo/form.jsp";
	
	private final int limiteEntrega = 10;
	private final int limiteRenova = 15;

	public EmprestimoServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Empresta empresta = new Empresta();
		DateTime hoje = new DateTime(new Date());
		if ("salvar".equals(action)) {
			DateTime entrega = hoje.plusDays(limiteEntrega);
			
			empresta.getAluno().setIdAluno(Long.valueOf(request.getParameter("id_aluno")));
			empresta.getLivro().setIdLivro(Long.valueOf(request.getParameter("id_livro")));
			
			empresta.setRetirada(new java.sql.Date(hoje.toDate().getTime()));
			empresta.setPrevisao(new java.sql.Date(entrega.toDate().getTime()));
			empresta.setEntrega(null);

			Long idEmpresta = EmprestaDAO.emprestar(empresta);
			request.setAttribute("empresta", EmprestaDAO.buscarId(idEmpresta));
			
		} else if ("renovar".equals(action)) {
			
			Long idEmpresta = Long.valueOf(request.getParameter("id_empresta"));
			DateTime renovado = hoje.plusDays(limiteRenova);
			
			EmprestaDAO.renovar(idEmpresta, new java.sql.Date(renovado.toDate().getTime()));
			
		} else if ("devolver".equals(action)) {
			Long idEmpresta = Long.valueOf(request.getParameter("id_empresta"));
			EmprestaDAO.devolver(idEmpresta);			
		}
				
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		RequestDispatcher rd;
		
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);			
		}
		
		request.setAttribute("livros", LivroDAO.buscarTodosDisponiveis());
		request.setAttribute("alunos", AlunoDAO.buscarTodosAtivos());
		request.setAttribute("emprestimos", EmprestaDAO.buscarPendentes());
		
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}

}