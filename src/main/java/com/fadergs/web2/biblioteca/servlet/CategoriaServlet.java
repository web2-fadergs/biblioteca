package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.CategoriaDAO;
import com.fadergs.web2.biblioteca.entity.Categoria;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorCategoria
 */
@WebServlet("/categorias")
public class CategoriaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CategoriaServlet.class);

	private static final String LIST_PAGE = "/categoria/list.jsp";
	private static final String FORM_PAGE = "/categoria/form.jsp";

	public CategoriaServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Categoria categoria = new Categoria();

		if ("salvar".equals(action)) {
			if (request.getParameter("id_categoria") != null && !"".equals(request.getParameter("id_categoria"))) {
				categoria.setIdCategoria(Long.valueOf(request.getParameter("id_categoria")));
			}
			categoria.setDescricao(request.getParameter("descricao"));
			CategoriaDAO.salvar(categoria);
		}
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		List<Categoria> categorias = null;
		
		RequestDispatcher rd;
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);			
		} else if ("editar".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
			
			Long idCategoria = Long.valueOf(request.getParameter("id_categoria"));			
			Categoria categoria = CategoriaDAO.buscarId(idCategoria);
			request.setAttribute("categoria", categoria);
		} else if ("deletar".equals(action)) {
			rd = request.getRequestDispatcher(LIST_PAGE);			
			Long idCategoria = Long.valueOf(request.getParameter("id_categoria"));
			CategoriaDAO.deletar(idCategoria);
			
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);
		}

		if ("listar".equals(action)) {
			String pesquisa = request.getParameter("pesquisa");
			categorias = CategoriaDAO.buscarDescricao(pesquisa);
		} else {
			categorias = CategoriaDAO.buscarTodos();
		}
		
		request.setAttribute("categorias", categorias);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}
}