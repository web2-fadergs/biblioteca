package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.AlunoDAO;
import com.fadergs.web2.biblioteca.dao.LivroDAO;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorAluno
 */
@WebServlet("/relatorios")
public class RelatorioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RelatorioServlet.class);

	private static final String EMPRESTIMOS_LIVRO_PAGE = "/relatorios/emprestimo-livros.jsp";
	private static final String EMPRESTIMOS_ALUNO_PAGE = "/relatorios/emprestimo-alunos.jsp";
	private static final String QUANTIDADE_PAGE = "/relatorios/quantidade-livros.jsp";

	public RelatorioServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		
		RequestDispatcher rd;
		
		if ("contador".equals(action)) {
			request.setAttribute("alunosAtivos", AlunoDAO.relatorioQuantidade(true));
			request.setAttribute("alunosInativos", AlunoDAO.relatorioQuantidade(false));
			rd = request.getRequestDispatcher(QUANTIDADE_PAGE);			
		} else if ("aluno".equals(action)){
			Long idAluno = null;
			if (request.getParameter("id_aluno") != null && !"".equals((request.getParameter("id_aluno")))) {
				idAluno = Long.valueOf(request.getParameter("id_aluno"));
			}
			request.setAttribute("todosAlunos", AlunoDAO.relatorioEmprestimos(null));
			request.setAttribute("movimentacaoAlunos", AlunoDAO.relatorioEmprestimos(idAluno));
			rd = request.getRequestDispatcher(EMPRESTIMOS_ALUNO_PAGE);
		} else {
			Long idLivro = null;
			if (request.getParameter("id_livro") != null && !"".equals((request.getParameter("id_livro")))) {
				idLivro = Long.valueOf(request.getParameter("id_livro"));
			}
			request.setAttribute("todosLivros", LivroDAO.relatorioEmprestimos(null));
			request.setAttribute("movimentacaoLivros", LivroDAO.relatorioEmprestimos(idLivro));
			rd = request.getRequestDispatcher(EMPRESTIMOS_LIVRO_PAGE);
		}
		
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}

}