package com.fadergs.web2.biblioteca.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.dao.AlunoDAO;
import com.fadergs.web2.biblioteca.entity.Aluno;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

/**
 * Servlet implementation class ControladorAluno
 */
@WebServlet("/alunos")
public class AlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(AlunoServlet.class);

	private static final String LIST_PAGE = "/alunos/list.jsp";
	private static final String FORM_PAGE = "/alunos/form.jsp";

	public AlunoServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "service";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		Aluno aluno = new Aluno();

		if ("salvar".equals(action)) {
			Boolean situacao = true;
			if (request.getParameter("id_aluno") != null && !"".equals(request.getParameter("id_aluno"))) {
				aluno.setIdAluno(Long.valueOf(request.getParameter("id_aluno")));
			}
			
			aluno.setNome(request.getParameter("nome"));
			aluno.setEndereco(request.getParameter("endereco"));
			aluno.setSituacao(situacao);
			
			AlunoDAO.salvar(aluno);
		}
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doGet()";
		LoggingUtility.logEntering(logger, methodName);
		
		String action = request.getParameter("action");
		List<Aluno> alunos = null;
		
		RequestDispatcher rd;
		if ("form".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);			
		} else if ("editar".equals(action)) {
			rd = request.getRequestDispatcher(FORM_PAGE);
			
			Long idAluno = Long.valueOf(request.getParameter("id_aluno"));			
			Aluno aluno = AlunoDAO.buscarId(idAluno);
			request.setAttribute("aluno", aluno);
		} else {
			rd = request.getRequestDispatcher(LIST_PAGE);
		}

		if ("listar".equals(action)) {
			String pesquisa = request.getParameter("pesquisa");
			alunos = AlunoDAO.buscarNome(pesquisa);
		} else {
			alunos = AlunoDAO.buscarTodos();
		}
		
		request.setAttribute("alunos", alunos);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String methodName = "doPost()";
		LoggingUtility.logEntering(logger, methodName);
		doGet(request, response);
	}

}