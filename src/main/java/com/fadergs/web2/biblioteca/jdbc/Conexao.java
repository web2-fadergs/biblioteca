package com.fadergs.web2.biblioteca.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

import org.postgresql.Driver;

public class Conexao {

	
	public static Connection getConnection() {
		
		Connection conn = null;
		try {
			DriverManager.registerDriver(new Driver());
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/biblioteca", "postgres", "postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
	public static void close(Connection conn) {
		
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}