package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Categoria;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class CategoriaDAO {
	private static final Logger logger = Logger.getLogger(BibliotecaDAO.class);
	private static Connection conexao = Conexao.getConnection();

	public static void salvar(Categoria categoria) {
		final String methodName = "salvar(categoria)";
		LoggingUtility.logEntering(logger, methodName, categoria.toString());
		
		if (categoria.getIdCategoria() == null) {
			cadastrar(categoria);
		} else {
			editar(categoria);
		}		
	}
	
	private static void cadastrar(Categoria categoria) {
		String sql = "INSERT INTO CATEGORIA (descricao) values (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, categoria.getDescricao());

			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void editar(Categoria categoria) {

		String sql = "UPDATE categoria SET descricao=(?) WHERE id_categoria = (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, categoria.getDescricao());
			preparador.setLong(2, categoria.getIdCategoria());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deletar(Long idCategoria) {
		try {
			String sql = "delete from categoria where id_categoria = (?)";

			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idCategoria);
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Categoria> buscarTodos() {

		String sql = "SELECT * FROM CATEGORIA";
		List<Categoria> lista = new ArrayList<Categoria>();
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {

				Categoria categ = new Categoria();
				categ.setIdCategoria(resultado.getLong("id_categoria"));
				categ.setDescricao(resultado.getString("descricao"));

				lista.add(categ);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static List<Categoria> buscarDescricao(String descricao) {
		List<Categoria> lista = new ArrayList<Categoria>();

		try {
			String sql = "SELECT * FROM CATEGORIA WHERE upper(descricao) LIKE ?";

			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, "%" + descricao.toUpperCase() + "%");

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Categoria categ = new Categoria();
				categ.setIdCategoria(resultado.getLong("id_categoria")); 
				categ.setDescricao(resultado.getString("descricao"));
				lista.add(categ);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static Categoria buscarId(Long idCategoria) {

		String sql = "SELECT * FROM CATEGORIA where id_categoria = ?";
		List<Categoria> lista = new ArrayList<Categoria>();
		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idCategoria);
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Categoria categ = new Categoria();
				categ.setIdCategoria(resultado.getLong("id_categoria"));
				categ.setDescricao(resultado.getString("descricao"));

				lista.add(categ);
			}

			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista.get(0);
	}

}