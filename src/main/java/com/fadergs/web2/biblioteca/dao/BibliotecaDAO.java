package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Biblioteca;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class BibliotecaDAO {
	private static final Logger logger = Logger.getLogger(BibliotecaDAO.class);
	private static Connection conexao = Conexao.getConnection();
	
	public static void salvar(Biblioteca biblioteca) {
		final String methodName = "salvar(biblioteca)";
		LoggingUtility.logEntering(logger, methodName, biblioteca.toString());
		
		if (biblioteca.getIdBiblioteca() == null) {
			cadastrar(biblioteca);
		} else {
			editar(biblioteca);
		}		
	}
	
	private static void cadastrar(Biblioteca biblioteca) {

		String sql = "INSERT INTO BIBLIOTECA (nome, endereco) values (?, ?)";
		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, biblioteca.getNome());
			preparador.setString(2, biblioteca.getEndereco());

			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void editar(Biblioteca biblioteca) {

		String sql = "UPDATE biblioteca SET nome=(?), endereco=(?) WHERE id_biblioteca = (?)";

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, biblioteca.getNome());
			preparador.setString(2, biblioteca.getEndereco());
			preparador.setLong(3, biblioteca.getIdBiblioteca());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void deletar(Long idBiblioteca) {

		String sql = "delete from biblioteca WHERE id_biblioteca = (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idBiblioteca);
			preparador.execute();
			
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Biblioteca> buscarTodos() {

		String sql = "SELECT * FROM BIBLIOTECA  order by nome";
		List<Biblioteca> lista = new ArrayList<Biblioteca>();
		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Biblioteca biblio = new Biblioteca();
				biblio.setIdBiblioteca(resultado.getLong("id_biblioteca"));
				biblio.setNome(resultado.getString("nome")); 
				biblio.setEndereco(resultado.getString("endereco"));
				
				lista.add(biblio);
			}
			preparador.close();
		} catch (SQLException e) {

			e.printStackTrace();

		}

		return lista;
	}

	public static Biblioteca buscarId(Long idBiblioteca) {

		String sql = "SELECT * FROM BIBLIOTECA where id_biblioteca = ?";
		ArrayList<Biblioteca> lista = new ArrayList<Biblioteca>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idBiblioteca);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Biblioteca biblio = new Biblioteca();

				biblio.setIdBiblioteca(resultado.getLong("id_biblioteca"));
				biblio.setNome(resultado.getString("nome"));
				biblio.setEndereco(resultado.getString("endereco"));

				lista.add(biblio);
			}

			preparador.close();
		} catch (SQLException e) {

			e.printStackTrace();

		}

		return lista.get(0);
	}

	public static List<Biblioteca> buscarNome(String nome) {

		String sql = "SELECT * FROM BIBLIOTECA where upper(nome) like ? order by nome";
		List<Biblioteca> lista = new ArrayList<Biblioteca>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, "%" + nome.toUpperCase() + "%");

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Biblioteca biblio = new Biblioteca();

				biblio.setIdBiblioteca(resultado.getLong("id_biblioteca"));
				biblio.setNome(resultado.getString("nome"));
				biblio.setEndereco(resultado.getString("endereco"));

				lista.add(biblio);
			}

			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}
}
