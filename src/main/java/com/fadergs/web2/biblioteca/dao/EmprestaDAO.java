package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Empresta;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class EmprestaDAO {
	private static final Logger logger = Logger.getLogger(LivroDAO.class);
	private static Connection conexao = Conexao.getConnection();
	
	private static final String SQL_LIST_EMPRESTA = "SELECT e.* " + 
			"  , a.nome as aluno_nome, a.situacao as aluno_situacao " + 
			"  , l.titulo as livro_titulo, a.situacao as livro_situacao, l.editora as livro_editora, l.valor " + 
			"FROM EMPRESTA e " + 
			"join aluno a on a.id_aluno = e.id_aluno " + 
			"join livro l on l.id_livro = e.id_livro";
	
	private static final String SQL_ATUALIZA_SITUACAO_ALUNOS = "update aluno set situacao = false " + 
			"where id_aluno in ( " + 
			"  SELECT a.id_aluno " + 
			"  from aluno a " + 
			"  join empresta e on a.id_aluno = e.id_aluno " + 
			"  where e.entrega is null and e.previsao < current_date " + 
			"  GROUP BY a.id_aluno " + 
			")";
	
	private static final String SQL_ATUALIZA_SITUACAO_LIVROS = "update livro set situacao = false " + 
			"where id_livro in ( " + 
			"  SELECT l.id_livro " + 
			"  from livro l " + 
			"  join empresta e on e.id_livro = l.id_livro " + 
			"  where e.entrega is null " + 
			"  GROUP BY l.id_livro " + 
			")";
	

	private static Empresta buildfromRS(ResultSet rs) throws SQLException {
		
		Empresta empresta = new Empresta();

		empresta.setIdEmpresta(rs.getLong("id_empresta"));
		empresta.getAluno().setIdAluno(rs.getLong("id_aluno"));
		empresta.getAluno().setNome(rs.getString("aluno_nome"));
		empresta.getAluno().setSituacao(rs.getString("aluno_situacao"));
		empresta.getLivro().setIdLivro(rs.getLong("id_livro"));
		empresta.getLivro().setTitulo(rs.getString("livro_titulo"));
		empresta.getLivro().setSituacao(rs.getBoolean("livro_situacao"));
		empresta.getLivro().setEditora(rs.getString("livro_editora"));
		empresta.getLivro().setValor(rs.getDouble("valor"));
		empresta.setRetirada(rs.getDate("retirada"));
		empresta.setPrevisao(rs.getDate("previsao"));
		empresta.setEntrega(rs.getDate("entrega"));
		
		return empresta;
	}
	
	public static Long emprestar(Empresta empresta) {
		
		final String methodName = "emprestar()";
		Long lastId = 0L;
		
		try {
			conexao.setAutoCommit(false);
			
			String sql = "INSERT INTO empresta (id_aluno, id_livro, retirada, previsao) values (?, ?, ?, ?)";
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, empresta.getAluno().getIdAluno());
			preparador.setLong(2, empresta.getLivro().getIdLivro());
			preparador.setDate(3, empresta.getRetirada());
			preparador.setDate(4, empresta.getPrevisao());
			preparador.execute();

			sql = "update livro set situacao = true";
			preparador = conexao.prepareStatement(sql);
			preparador.execute();
			
			sql = SQL_ATUALIZA_SITUACAO_LIVROS;
			preparador = conexao.prepareStatement(sql);
			preparador.execute();
			
			sql = "update aluno set situacao = true";
			preparador = conexao.prepareStatement(sql);
			preparador.execute();		
			
			sql = SQL_ATUALIZA_SITUACAO_ALUNOS;
			preparador = conexao.prepareStatement(sql);
			preparador.execute();
			
			sql = "SELECT MAX(ID_EMPRESTA) as ULTIMO FROM EMPRESTA";
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			
			while (resultado.next()) {
				lastId = resultado.getLong("ULTIMO");
				empresta.setIdEmpresta(lastId);
			}
			conexao.commit();
			LoggingUtility.logDebug(logger, methodName, "Emprestimo Cadastrado = [" + empresta + "]");
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
				conexao.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return lastId;
	}

	public static Empresta buscarId(Long idEmpresta) {
		Empresta empresta = null;
				
		try {
			String sql = SQL_LIST_EMPRESTA + " where e.id_empresta = ? order by l.titulo";
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idEmpresta);
			ResultSet resultado = preparador.executeQuery();
			
			while (resultado.next()) {
				empresta = buildfromRS(resultado);
			}
			
			preparador.close();
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
				conexao.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return empresta;	
	}
	
	public static void renovar(Long idEmpresta, Date previsao) {
		try {
			String sql = "UPDATE empresta SET previsao = ?, entrega = null WHERE id_empresta = ? ";
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setDate(1, previsao);
			preparador.setLong(2, idEmpresta);
			preparador.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void devolver(Long idEmpresta) {
		try {
			Empresta empresta = buscarId(idEmpresta); 			

			conexao.setAutoCommit(false);			
			
			String sql = "UPDATE empresta SET entrega = current_date WHERE id_empresta = ? ";
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, empresta.getIdEmpresta());
			preparador.execute();

			sql = "update livro set situacao = true";
			preparador = conexao.prepareStatement(sql);
			preparador.execute();
			
			sql = SQL_ATUALIZA_SITUACAO_LIVROS;
			preparador = conexao.prepareStatement(sql);
			preparador.execute();

			sql = "update aluno set situacao = true";
			preparador = conexao.prepareStatement(sql);
			preparador.execute();		
			
			sql = SQL_ATUALIZA_SITUACAO_ALUNOS;
			preparador = conexao.prepareStatement(sql);
			preparador.execute();
			
			conexao.commit();
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
				conexao.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public static List<Empresta> buscarTodos() {

		String sql = SQL_LIST_EMPRESTA + " order by l.titulo";
		List<Empresta> lista = new ArrayList<Empresta>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {
				lista.add(buildfromRS(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	public static List<Empresta> buscarPendentes() {

		String sql = SQL_LIST_EMPRESTA + " where e.entrega is null order by e.retirada desc, l.titulo";
		List<Empresta> lista = new ArrayList<Empresta>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {
				lista.add(buildfromRS(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

}
