package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Aluno;
import com.fadergs.web2.biblioteca.entity.Empresta;
import com.fadergs.web2.biblioteca.entity.Livro;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class AlunoDAO {

	private static final Logger logger = Logger.getLogger(AlunoDAO.class);
	private static Connection conexao = Conexao.getConnection();

	private static final String SQL_RELATORIO_EMPRESTIMOS = "SELECT l.id_livro, l.id_biblioteca, l.id_categoria, l.titulo, l.editora, l.situacao as livro_situacao, l.valor " + 
			"      ,c.descricao as categoria_descricao " + 
			"      ,b.nome as biblioteca_nome, b.endereco as biblioteca_endereco " + 
			"      ,e.id_empresta,e.entrega, e.retirada, e.previsao, e.id_aluno " + 
			"      ,a.nome, a.endereco, a.situacao " + 
			"from livro l " + 
			"join categoria c on c.id_categoria = l.id_categoria " + 
			"join biblioteca b on b.id_biblioteca = l.id_biblioteca " + 
			"join empresta e on e.id_livro = l.id_livro " + 
			"join aluno    a on a.id_aluno = e.id_aluno";
	
	private static Aluno buildAluno(ResultSet rs) throws SQLException {
		Aluno aluno = new Aluno();
		aluno.setIdAluno(rs.getLong("id_aluno"));
		aluno.setNome(rs.getString("nome"));
		aluno.setEndereco(rs.getString("endereco"));
		aluno.setSituacao(rs.getBoolean("situacao"));
		
		return aluno;
	}
	
	private static Livro buildLivro(ResultSet rs) throws SQLException  {
		Livro livro = new Livro();
		livro.setIdLivro(rs.getLong("id_livro"));
		livro.setTitulo(rs.getString("titulo"));
		livro.setEditora(rs.getString("editora"));
		livro.setValor(rs.getDouble("valor"));
		livro.getCategoria().setIdCategoria(rs.getLong("id_categoria"));
		livro.getCategoria().setDescricao(rs.getString("categoria_descricao"));
		livro.getBiblioteca().setIdBiblioteca(rs.getLong("id_biblioteca"));
		livro.getBiblioteca().setNome(rs.getString("biblioteca_nome"));
		livro.getBiblioteca().setEndereco(rs.getString("biblioteca_endereco"));
		livro.setSituacao(rs.getBoolean("livro_situacao"));
		
		return livro;
	}
	
	private static Empresta buildEmpresta(ResultSet rs) throws SQLException {
		Empresta emprestimo = new Empresta();
		emprestimo.setIdEmpresta(rs.getLong("id_empresta"));
		emprestimo.setRetirada(rs.getDate("retirada"));
		emprestimo.setPrevisao(rs.getDate("previsao"));
		emprestimo.setEntrega(rs.getDate("entrega"));
		emprestimo.setLivro(buildLivro(rs));
				
		return emprestimo;
	}
	
	public static void salvar(Aluno aluno) {
		final String methodName = "salvar(aluno)";
		LoggingUtility.logEntering(logger, methodName, aluno.toString());
		
		if (aluno.getIdAluno() == null) {
			cadastrar(aluno);
		} else {
			editar(aluno);
		}		
	}
	
	private static void cadastrar(Aluno aluno) {

		String sql = "INSERT INTO ALUNO (nome, endereco, situacao) values (?, ?, ?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, aluno.getNome());
			preparador.setString(2, aluno.getEndereco());
			preparador.setBoolean(3, aluno.getSituacao());

			preparador.execute();
			preparador.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void editar(Aluno aluno) {

		String sql = "UPDATE aluno SET nome=(?), endereco=(?) WHERE id_aluno = (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, aluno.getNome());
			preparador.setString(2, aluno.getEndereco());
			preparador.setLong(3, aluno.getIdAluno());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void deletar(Long idAluno) {

		String sql = "delete from aluno WHERE id_aluno = (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idAluno);
			preparador.execute();
			
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Aluno> relatorioQuantidade(Boolean ativo) {

		String sql = "SELECT a.*" + 
				"    , count(e.id_empresta) quantidadeLivros " + 
				"from aluno a " + 
				"join empresta e on e.id_aluno = a.id_aluno " + 
				"where e.entrega is null and a.situacao = ? " + 
				"GROUP BY a.id_aluno " + 
				"order by a.nome ";
		
		List<Aluno> lista = new ArrayList<Aluno>();
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setBoolean(1, ativo);
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				Aluno aluno = buildAluno(resultado);
				aluno.setQuantidadeLivros(resultado.getLong("quantidadeLivros"));
				lista.add(aluno);
			}
			
			preparador.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		return lista;
	}
	
	public static List<Aluno> buscarTodos() {
		final String methodName = "buscarTodos()";
		LoggingUtility.logEntering(logger, methodName);
		
		String sql = "SELECT * FROM ALUNO order by nome";
		List<Aluno> lista = new ArrayList<Aluno>();
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {
				lista.add(buildAluno(resultado));
			}
			
			preparador.close();
			LoggingUtility.logDebug(logger, methodName, "Pesquisado com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static List<Aluno> buscarTodosAtivos() {

		String sql = "SELECT * FROM ALUNO WHERE SITUACAO = true order by nome";
		List<Aluno> lista = new ArrayList<Aluno>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {
				lista.add(buildAluno(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static List<Aluno> buscarNome(String nome) {
		final String methodName = "buscarNome(nome)";
		LoggingUtility.logEntering(logger, methodName, nome);
		
		List<Aluno> lista = new ArrayList<Aluno>();

		try {
			String sql = "SELECT * FROM ALUNO WHERE upper(nome) LIKE ? order by nome";
			

			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, "%" + nome.toUpperCase() + "%");

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				lista.add(buildAluno(resultado));
			}

			preparador.close();
			LoggingUtility.logDebug(logger, methodName, "Pesquisado com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static Aluno buscarId(Long idAluno) {

		String sql = "SELECT * FROM ALUNO where id_aluno = ?";
		List<Aluno> lista = new ArrayList<Aluno>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idAluno);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				lista.add(buildAluno(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista.get(0);
	}
	
	public static List<Aluno> relatorioEmprestimos(Long idAluno) {
		List<Aluno> lista = new ArrayList<Aluno>();

		try {
			String sql = SQL_RELATORIO_EMPRESTIMOS;
			String where = " where a.id_aluno = ? ";
			String order = " order by a.nome, e.entrega desc, e.retirada desc ";
			
			if (idAluno != null) {
				sql = sql + where;				
			}
			sql = sql + order;

			PreparedStatement preparador = conexao.prepareStatement(sql);
			if (idAluno != null) {
				preparador.setLong(1, idAluno);
			}

			ResultSet resultado = preparador.executeQuery();

			Long idAtual = 0L;
			Aluno aluno = null;
			while (resultado.next()) {
				Long id = resultado.getLong("id_aluno");
				
				if (idAtual != id) {
					if (!idAtual.equals(0l)) {
						lista.add(aluno);
					}
					idAtual = id;
					aluno = buildAluno(resultado);
				}
				
				if (aluno != null && idAtual == aluno.getIdAluno()) {
					aluno.getEmprestimos().add(buildEmpresta(resultado));
				}
			}
			if (aluno != null) {
				lista.add(aluno);
			}
			
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}
	
}