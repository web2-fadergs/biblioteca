package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Funcionario;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class FuncionarioDAO {
	private static final Logger logger = Logger.getLogger(FuncionarioDAO.class);
	private static Connection conexao = Conexao.getConnection();

	private static final String SQL_LIST_FUNCIONARIOS = "select f.id_funcionario, f.id_biblioteca, f.nome, f.endereco, f.salario, f.telefone, b.nome as nome_biblioteca, b.endereco as endereco_biblioteca " + 
			"from funcionario f " + 
			"join biblioteca b on b.id_biblioteca = f.id_biblioteca";

	public static void salvar(Funcionario funcionario) {
		final String methodName = "salvar(biblioteca)";
		LoggingUtility.logEntering(logger, methodName, funcionario.toString());
		
		if (funcionario.getIdFuncionario() == null) {
			cadastrar(funcionario);
		} else {
			editar(funcionario);
		}	
	}
	
	private static void cadastrar(Funcionario funcionario) {
		String sql = "INSERT INTO FUNCIONARIO (nome, endereco, telefone, salario, id_biblioteca) values (?, ?, ?, ?, ?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, funcionario.getNome());
			preparador.setString(2, funcionario.getEndereco());
			preparador.setString(3, funcionario.getTelefone());
			preparador.setDouble(4, funcionario.getSalario());
			preparador.setLong(5, funcionario.getBiblioteca().getIdBiblioteca());

			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void editar(Funcionario funcionario) {
		String sql = "UPDATE funcionario SET nome=(?), endereco=(?), telefone=(?), salario=(?), id_biblioteca=(?) WHERE id_funcionario = (?)";

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, funcionario.getNome());
			preparador.setString(2, funcionario.getEndereco());
			preparador.setString(3, funcionario.getTelefone());
			preparador.setDouble(4, funcionario.getSalario());
			preparador.setLong(5, funcionario.getBiblioteca().getIdBiblioteca());
			preparador.setLong(6, funcionario.getIdFuncionario());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deletar(Long idFuncionario) {
		try {
			String sql = "delete from funcionario where id_funcionario = (?)";
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idFuncionario);
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Funcionario> buscarTodos() {

		String sql = SQL_LIST_FUNCIONARIOS + " order by nome";
		ArrayList<Funcionario> lista = new ArrayList<Funcionario>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Funcionario funcionario = new Funcionario();

				funcionario.setIdFuncionario(resultado.getLong("id_funcionario"));
				funcionario.setNome(resultado.getString("nome"));
				funcionario.setEndereco(resultado.getString("endereco"));
				funcionario.setTelefone(resultado.getString("telefone"));
				funcionario.setSalario(resultado.getDouble("salario"));
				funcionario.getBiblioteca().setIdBiblioteca(resultado.getLong("id_biblioteca"));
				funcionario.getBiblioteca().setNome(resultado.getString("nome_biblioteca"));
				funcionario.getBiblioteca().setEndereco(resultado.getString("endereco_biblioteca"));

				lista.add(funcionario);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static Funcionario buscarId(Long idFuncionario) {

		String sql = SQL_LIST_FUNCIONARIOS + " WHERE f.id_funcionario = ?  order by f.nome";
		ArrayList<Funcionario> lista = new ArrayList<Funcionario>();
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idFuncionario);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Funcionario funcionario = new Funcionario();

				funcionario.setIdFuncionario(resultado.getLong("id_funcionario"));
				funcionario.setNome(resultado.getString("nome"));
				funcionario.setEndereco(resultado.getString("endereco"));
				funcionario.setTelefone(resultado.getString("telefone"));
				funcionario.setSalario(resultado.getDouble("salario"));
				funcionario.getBiblioteca().setIdBiblioteca(resultado.getLong("id_biblioteca"));
				funcionario.getBiblioteca().setNome(resultado.getString("nome_biblioteca"));
				funcionario.getBiblioteca().setEndereco(resultado.getString("endereco_biblioteca"));

				lista.add(funcionario);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista.get(0);
	}

	public static ArrayList<Funcionario> buscarNome(String nome) {

		ArrayList<Funcionario> lista = new ArrayList<Funcionario>();

		try {
			String sql = SQL_LIST_FUNCIONARIOS + " WHERE upper(f.nome) LIKE ? order by f.nome";

			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, "%" + nome.toUpperCase() + "%");

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {

				Funcionario funcionario = new Funcionario();

				funcionario.setIdFuncionario(resultado.getLong("id_funcionario"));
				funcionario.setNome(resultado.getString("nome"));
				funcionario.setEndereco(resultado.getString("endereco"));
				funcionario.setTelefone(resultado.getString("telefone"));
				funcionario.setSalario(resultado.getDouble("salario"));
				funcionario.getBiblioteca().setIdBiblioteca(resultado.getLong("id_biblioteca"));
				funcionario.getBiblioteca().setNome(resultado.getString("nome_biblioteca"));
				funcionario.getBiblioteca().setEndereco(resultado.getString("endereco_biblioteca"));

				lista.add(funcionario);
			}

			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

}