package com.fadergs.web2.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.fadergs.web2.biblioteca.entity.Empresta;
import com.fadergs.web2.biblioteca.entity.Livro;
import com.fadergs.web2.biblioteca.jdbc.Conexao;
import com.fadergs.web2.biblioteca.util.log.LoggingUtility;

public class LivroDAO {
	
	private static final Logger logger = Logger.getLogger(LivroDAO.class);
	private static Connection conexao = Conexao.getConnection();
	
	private static final String SQL_LIST_LIVROS = "select l.id_livro, l.id_biblioteca, l.id_categoria, l.titulo, l.editora, l.situacao, l.valor " + 
			"      ,b.nome as biblioteca_nome, b.endereco as biblioteca_endereco " + 
			"      ,c.descricao as categoria_descricao " + 
			"from livro l " + 
			"join biblioteca b on b.id_biblioteca = l.id_biblioteca " + 
			"join categoria c on c.id_categoria = l.id_categoria";
	
	private static final String SQL_RELATORIO_EMPRESTIMOS = "SELECT l.id_livro, l.id_biblioteca, l.id_categoria, l.titulo, l.editora, l.situacao, l.valor " + 
			"      ,c.descricao as categoria_descricao " + 
			"      ,b.nome as biblioteca_nome, b.endereco as biblioteca_endereco " + 
			"      ,e.id_empresta,e.entrega, e.retirada, e.previsao, e.id_aluno " + 
			"      ,a.nome as aluno_nome, a.situacao as aluno_situacao " + 
			"from livro l " + 
			"join categoria c on c.id_categoria = l.id_categoria " + 
			"join biblioteca b on b.id_biblioteca = l.id_biblioteca " + 
			"join empresta e on e.id_livro = l.id_livro " + 
			"join aluno    a on a.id_aluno = e.id_aluno";
	
	private static final String SQL_LIST_PREVISAO = "SELECT e.previsao " + 
			"from livro l " + 
			"join empresta e on e.id_livro = l.id_livro " + 
			"where e.entrega is null";
	
	private static Livro buildLivro(ResultSet rs) throws SQLException  {
		Livro livro = new Livro();
		livro.setIdLivro(rs.getLong("id_livro"));
		livro.setTitulo(rs.getString("titulo"));
		livro.setEditora(rs.getString("editora"));
		livro.setValor(rs.getDouble("valor"));
		livro.getCategoria().setIdCategoria(rs.getLong("id_categoria"));
		livro.getCategoria().setDescricao(rs.getString("categoria_descricao"));
		livro.getBiblioteca().setIdBiblioteca(rs.getLong("id_biblioteca"));
		livro.getBiblioteca().setNome(rs.getString("biblioteca_nome"));
		livro.getBiblioteca().setEndereco(rs.getString("biblioteca_endereco"));
		livro.setSituacao(rs.getBoolean("situacao"));
		
		return livro;
	}
	
	private static Empresta buildEmpresta(ResultSet rs) throws SQLException {
		Empresta emprestimo = new Empresta();
		emprestimo.setIdEmpresta(rs.getLong("id_empresta"));
		emprestimo.setRetirada(rs.getDate("retirada"));
		emprestimo.setPrevisao(rs.getDate("previsao"));
		emprestimo.setEntrega(rs.getDate("entrega"));
		
		emprestimo.getAluno().setIdAluno(rs.getLong("id_aluno"));
		emprestimo.getAluno().setNome(rs.getString("aluno_nome"));
		emprestimo.getAluno().setSituacao(rs.getBoolean("aluno_situacao"));
		
		return emprestimo;
	}
	
	public static void salvar(Livro livro) {
		final String methodName = "salvar(livro)";
		LoggingUtility.logEntering(logger, methodName, livro.toString());
		
		if (livro.getIdLivro() == null) {
			cadastrar(livro);
		} else {
			editar(livro);
		}		
	}
	
	public static void cadastrar(Livro livro) {
		
		String sql = "INSERT INTO LIVRO (titulo, editora, valor, id_categoria, id_biblioteca, situacao) values (?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, livro.getTitulo());
			preparador.setString(2, livro.getEditora());
			preparador.setDouble(3, livro.getValor());
			preparador.setLong(4, livro.getCategoria().getIdCategoria());
			preparador.setLong(5, livro.getBiblioteca().getIdBiblioteca());
			preparador.setBoolean(6, livro.getSituacao());

			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editar(Livro livro) {
		String sql = "UPDATE livro SET titulo=(?), editora=(?), valor=(?), id_categoria=(?), id_biblioteca=(?) WHERE id_livro = (?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, livro.getTitulo());
			preparador.setString(2, livro.getEditora());
			preparador.setDouble(3, livro.getValor());
			preparador.setLong(4, livro.getCategoria().getIdCategoria());
			preparador.setLong(5, livro.getBiblioteca().getIdBiblioteca());
			preparador.setLong(6, livro.getIdLivro());

			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deletar(Long idLivro) {

		String sql = "delete from livro WHERE id_livro = (?) and situacao = true";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idLivro);
			preparador.execute();
			
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static List<Livro> buscarTodos() {

		String sql = SQL_LIST_LIVROS + " ORDER BY L.TITULO ";
		List<Livro> lista = new ArrayList<Livro>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				Livro livro = buildLivro(resultado);
				if(!livro.getSituacao()) {
					livro.setPrevisaoRetorno(getPrevisaoRetorno(livro.getIdLivro()));
				}
				lista.add(livro);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	private static Date getPrevisaoRetorno(Long idLivro) {
		String sql = SQL_LIST_PREVISAO + " AND l.id_livro = ? ";
		
		Date retorno = null;
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idLivro);
			
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				retorno = resultado.getDate("previsao");
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retorno;
	}

	public static List<Livro> buscarTodosDisponiveis() {

		String sql = SQL_LIST_LIVROS + " WHERE l.id_livro NOT IN (SELECT id_livro FROM EMPRESTA WHERE entrega IS NULL) ORDER BY L.TITULO ";
		List<Livro> lista = new ArrayList<Livro>();

		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				lista.add(buildLivro(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public static Livro buscarId(Long idLivro) {

		String sql = SQL_LIST_LIVROS + " where l.id_livro = ? ORDER BY L.TITULO ";
		List<Livro> lista = new ArrayList<Livro>();
		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setLong(1, idLivro);

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				lista.add(buildLivro(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista.get(0);
	}

	public static List<Livro> buscarTitulo(String titulo) {
		List<Livro> lista = new ArrayList<Livro>();

		try {
			String sql = SQL_LIST_LIVROS + " WHERE upper(l.titulo) LIKE ?  ORDER BY L.TITULO ";

			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, "%" + titulo.toUpperCase() + "%");

			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				lista.add(buildLivro(resultado));
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	public static List<Livro> relatorioEmprestimos(Long idLivro) {
		List<Livro> lista = new ArrayList<Livro>();

		try {
			String sql = SQL_RELATORIO_EMPRESTIMOS;
			String where = " where l.id_livro = ? ";
			String order = " order by l.titulo, e.entrega desc, e.retirada desc ";
			
			if (idLivro != null) {
				sql = sql + where;				
			}
			sql = sql + order;

			PreparedStatement preparador = conexao.prepareStatement(sql);
			if (idLivro != null) {
				preparador.setLong(1, idLivro);
			}

			ResultSet resultado = preparador.executeQuery();

			Long idAtual = 0L;
			Livro livro = null;
			while (resultado.next()) {
				Long id = resultado.getLong("id_livro");
				
				if (idAtual != id) {
					if (!idAtual.equals(0l)) {
						lista.add(livro);
					}
					idAtual = id;
					livro = buildLivro(resultado);
				}
				
				if (livro != null && idAtual == livro.getIdLivro()) {
					livro.getEmprestimos().add(buildEmpresta(resultado));
				}
			}
			if (livro != null) {
				lista.add(livro);
			}
			
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}

}