package com.fadergs.web2.biblioteca.entity;

public class Biblioteca {

    private Long idBiblioteca;
    private String nome;
    private String endereco;

    public Long getIdBiblioteca() {
        return idBiblioteca;
    }

    public void setIdBiblioteca(Long idBiblioteca) {
        this.idBiblioteca = idBiblioteca;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

	@Override
	public String toString() {
		return "Biblioteca [idBiblioteca=" + idBiblioteca + ", nome=" + nome + ", endereco=" + endereco + "]";
	}
}
