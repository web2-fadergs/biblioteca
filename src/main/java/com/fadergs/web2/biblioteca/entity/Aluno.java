package com.fadergs.web2.biblioteca.entity;

import java.util.ArrayList;
import java.util.List;

public class Aluno {

    private Long idAluno;
    private String nome;
    private String endereco;
    private Boolean situacao;
    private Long quantidadeLivros;
    
    private List<Empresta> emprestimos;

    public Long getIdAluno() {
        return idAluno;
    }

    public void setIdAluno(Long idAluno) {
        this.idAluno = idAluno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Boolean getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
    	if ("Ativo".equalsIgnoreCase(situacao)) {
    		setSituacao(true);
    	} else {
    		setSituacao(false);
    	}
	}
    
    public void setSituacao(Boolean situacao) {
        this.situacao = situacao;
    }
    
    public Long getQuantidadeLivros() {
		return quantidadeLivros;
	}

	public void setQuantidadeLivros(Long quantidadeLivros) {
		this.quantidadeLivros = quantidadeLivros;
	}

	public List<Empresta> getEmprestimos() {
		if (emprestimos == null) {
			emprestimos = new ArrayList<Empresta>();
    	}
		return emprestimos;
	}

	public void setEmprestimos(List<Empresta> emprestimos) {
		this.emprestimos = emprestimos;
	}

	@Override
	public String toString() {
		return "Aluno [idAluno=" + idAluno + ", nome=" + nome + ", endereco=" + endereco + ", situacao=" + situacao
				+ "]";
	}
}
