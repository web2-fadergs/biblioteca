package com.fadergs.web2.biblioteca.entity;

public class Categoria {

    private Long idCategoria;
    private String descricao;

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", descricao=" + descricao + "]";
	}
}
