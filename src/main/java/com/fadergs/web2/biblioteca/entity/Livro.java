package com.fadergs.web2.biblioteca.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Livro {

    private Long idLivro;
    private String titulo;
    private String editora;
    private Double valor;

    private Categoria categoria;

    private Biblioteca biblioteca;
    private Boolean situacao;
    
    private Date previsaoRetorno;

    private List<Empresta> emprestimos;
    
    
    public Long getIdLivro() {
        return idLivro;
    }

    public void setIdLivro(Long idLivro) {
        this.idLivro = idLivro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Categoria getCategoria() {
    	if (categoria == null) {
    		categoria = new Categoria();
    	}
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Biblioteca getBiblioteca() {
    	if (biblioteca == null) {
    		biblioteca = new Biblioteca();
    	}
        return biblioteca;
    }

    public void setBiblioteca(Biblioteca biblioteca) {
        this.biblioteca = biblioteca;
    }

    public Boolean getSituacao() {
        return situacao;
    }

    public void setSituacao(Boolean situacao) {
        this.situacao = situacao;
    }
    
	public List<Empresta> getEmprestimos() {
		if (emprestimos == null) {
			emprestimos = new ArrayList<Empresta>();
    	}
		return emprestimos;
	}

	public void setEmprestimos(List<Empresta> emprestimos) {
		this.emprestimos = emprestimos;
	}

	public Date getPrevisaoRetorno() {
		return previsaoRetorno;
	}

	public void setPrevisaoRetorno(Date previsaoRetorno) {
		this.previsaoRetorno = previsaoRetorno;
	}

	@Override
	public String toString() {
		return "Livro [idLivro=" + idLivro + ", titulo=" + titulo + ", editora=" + editora + ", valor=" + valor
				+ ", categoria=" + categoria + ", biblioteca=" + biblioteca + ", situacao=" + situacao + "]";
	}
}