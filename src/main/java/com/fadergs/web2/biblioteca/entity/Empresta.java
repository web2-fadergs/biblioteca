package com.fadergs.web2.biblioteca.entity;

import java.sql.Date;

public class Empresta {

    private Long idEmpresta;

    private Aluno aluno;
    private Livro livro;

    private Date retirada;
    private Date previsao;
    private Date entrega;


    public Long getIdEmpresta() {
        return idEmpresta;
    }

    public void setIdEmpresta(Long idEmpresta) {
        this.idEmpresta = idEmpresta;
    }

    public Aluno getAluno() {
    	if (aluno == null) {
    		aluno = new Aluno();
    	}
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Livro getLivro() {
    	if (livro == null) {
    		livro = new Livro();
    	}
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    public Date getRetirada() {
        return retirada;
    }

    public void setRetirada(Date retirada) {
        this.retirada = retirada;
    }

    public Date getPrevisao() {
        return previsao;
    }

    public void setPrevisao(Date previsao) {
        this.previsao = previsao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

	@Override
	public String toString() {
		return "Empresta [idEmpresta=" + idEmpresta + ", aluno=" + aluno + ", livro=" + livro + ", retirada=" + retirada
				+ ", previsao=" + previsao + ", entrega=" + entrega + "]";
	}
    
}
