package com.fadergs.web2.biblioteca.entity;

public class Funcionario {

    private Long idFuncionario;
    private String nome;
    private String endereco;
    private String telefone;
    private Double salario;

    private Biblioteca biblioteca;

    public Long getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(Long idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Biblioteca getBiblioteca() {
    	if (biblioteca == null) {
    		biblioteca = new Biblioteca();
    	}
        return biblioteca;
    }

    public void setBiblioteca(Biblioteca biblioteca) {
        this.biblioteca = biblioteca;
    }

	@Override
	public String toString() {
		return "Funcionario [idFuncionario=" + idFuncionario + ", nome=" + nome + ", endereco=" + endereco
				+ ", telefone=" + telefone + ", salario=" + salario + ", biblioteca=" + biblioteca + "]";
	}
    
}