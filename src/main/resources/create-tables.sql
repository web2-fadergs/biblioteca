create database biblioteca;

create table aluno
(
	id_aluno bigserial not null
		constraint aluno_pkey
			primary key,
	endereco varchar(255),
	nome varchar(255),
	situacao boolean
)
;

create table biblioteca
(
	id_biblioteca bigserial not null
		constraint biblioteca_pkey
			primary key,
	endereco varchar(255),
	nome varchar(255)
)
;

create table categoria
(
	id_categoria bigserial not null
		constraint categoria_pkey
			primary key,
	descricao varchar(255)
)
;

create table empresta
(
	id_empresta bigserial not null
		constraint empresta_pkey
			primary key,
	entrega date,
	previsao date,
	retirada date,
	id_aluno serial not null
		constraint fk3nj681wpc5wxi590b09nq7kux
			references aluno,
	id_livro serial not null
)
;

create table funcionario
(
	id_funcionario bigserial not null
		constraint funcionario_pkey
			primary key,
	endereco varchar(255),
	nome varchar(255),
	salario double precision,
	telefone varchar(255),
	id_biblioteca serial not null
		constraint fkh5kh7p9i5wdq2in82fctvu2ln
			references biblioteca
)
;

create table livro
(
	id_livro bigserial not null
		constraint livro_pkey
			primary key,
	editora varchar(255),
	situacao boolean,
	titulo varchar(255),
	valor double precision,
	id_biblioteca serial not null
		constraint fkr60r5ssp63nbumoogj24d4aw0
			references biblioteca,
	id_categoria serial not null
		constraint fksytxfd70p7l51i2cwndj1d21m
			references categoria
)
;

alter table empresta
	add constraint fk8qtg0bhkfg6l271k3mu8xkgia
		foreign key (id_livro) references livro
;