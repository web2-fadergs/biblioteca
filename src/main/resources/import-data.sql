-- biblioteca
INSERT INTO biblioteca (id_biblioteca, endereco, nome) VALUES (1, 'XV de Janeiro, 270', 'Biblioteca Municipal Canoas');

--funcionario
INSERT INTO funcionario (id_funcionario, endereco, nome, salario, telefone, id_biblioteca) VALUES (1, 'Silva So, 350', 'Marta', 1500, '5181069688', 1);
INSERT INTO funcionario (id_funcionario, endereco, nome, salario, telefone, id_biblioteca) VALUES (2, 'Dom Pedro II, 200', 'Paula', 4500, '5199874546', 1);
INSERT INTO funcionario (id_funcionario, endereco, nome, salario, telefone, id_biblioteca) VALUES (3, 'Ipiranga, 700', 'Carlos', 3000, '5181069545', 1);
INSERT INTO funcionario (id_funcionario, endereco, nome, salario, telefone, id_biblioteca) VALUES (4, 'Protasio Alves, 450', 'Jonas', 4500, '5187874452', 1);

--categoria
INSERT INTO categoria (id_categoria, descricao) VALUES (1, 'Ficcao');
INSERT INTO categoria (id_categoria, descricao) VALUES (2, 'Romance');
INSERT INTO categoria (id_categoria, descricao) VALUES (3, 'Literatura');
INSERT INTO categoria (id_categoria, descricao) VALUES (4, 'Cientifico');

--livro
INSERT INTO livro (id_livro, editora, situacao, titulo, valor, id_biblioteca, id_categoria) VALUES (1, 'XYZ', true, 'Senhor dos Aneis', 75, 1, 1);
INSERT INTO livro (id_livro, editora, situacao, titulo, valor, id_biblioteca, id_categoria) VALUES (2, 'XYZ', true, 'Harry Potter', 35, 1, 1);
INSERT INTO livro (id_livro, editora, situacao, titulo, valor, id_biblioteca, id_categoria) VALUES (3, 'ABC', true, 'O Cortico', 50, 1, 3);
INSERT INTO livro (id_livro, editora, situacao, titulo, valor, id_biblioteca, id_categoria) VALUES (4, 'DEF', true, 'Aprenda Quimica', 70, 1, 4);
INSERT INTO livro (id_livro, editora, situacao, titulo, valor, id_biblioteca, id_categoria) VALUES (5, 'FGH', false, 'Outro Lado da Meia Noite', 80, 1, 2);

--aluno
INSERT INTO aluno (id_aluno, endereco, nome, situacao) VALUES (1, 'Assis Brasil, 110', 'Maria Paula', true);
INSERT INTO aluno (id_aluno, endereco, nome, situacao) VALUES (2, 'Carlos Gomes, 150', 'Mauro Eduardo', true);
INSERT INTO aluno (id_aluno, endereco, nome, situacao) VALUES (3, 'Andradas, 1200', 'Joao Gilberto', false);
INSERT INTO aluno (id_aluno, endereco, nome, situacao) VALUES (4, 'Jose do Patrocinio, 500', 'Paulo Gomes', true);

--empresta
INSERT INTO empresta (id_empresta, entrega, previsao, retirada, id_aluno, id_livro) VALUES (1, '2017-11-10', '2017-11-20', '2017-11-10', 1, 1);
INSERT INTO empresta (id_empresta, entrega, previsao, retirada, id_aluno, id_livro) VALUES (2, '2017-11-18', '2017-11-20', '2017-11-10', 2, 2);
INSERT INTO empresta (id_empresta, entrega, previsao, retirada, id_aluno, id_livro) VALUES (3, '2017-11-14', '2017-11-21', '2017-11-11', 1, 1);
INSERT INTO empresta (id_empresta, entrega, previsao, retirada, id_aluno, id_livro) VALUES (4, '2017-12-24', '2017-11-01', '2017-10-21', 3, 5);

ALTER SEQUENCE biblioteca_id_biblioteca_seq RESTART WITH 2;
ALTER SEQUENCE funcionario_id_funcionario_seq RESTART WITH 5;
ALTER SEQUENCE categoria_id_categoria_seq RESTART WITH 5;
ALTER SEQUENCE livro_id_livro_seq RESTART WITH 6;
ALTER SEQUENCE aluno_id_aluno_seq RESTART WITH 5;
ALTER SEQUENCE empresta_id_empresta_seq RESTART WITH 5;


